import { createApp } from 'vue';
import App from './App.vue';
import router from './router';
import 'bootstrap/dist/css/bootstrap.css'

const app = createApp(App);

// Menggunakan router
app.use(router);

// Mounting aplikasi ke elemen dengan id "app" di HTML
app.mount('#app');
