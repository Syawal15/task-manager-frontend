import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/HomeView.vue';
import TaskList from '../components/TaskList.vue';
import TaskCreate from '../components/TaskCreate.vue';
import TaskEdit from '../components/TaskEdit.vue';
import UserLogin from '../components/UserLogin.vue';
import RegisterUser from '../components/RegisterUser.vue';
import UserLogout from '../components/UserLogout.vue';
import NotFound from '../components/NotFound.vue';

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/task',
    name: 'TaskList',
    component: TaskList,
    meta: { requiresAuth: true },
  },
  {
    path: '/create',
    name: 'TaskCreate',
    component: TaskCreate,
    meta: { requiresAuth: true },
  },
  {
    path: '/edit/:id',
    name: 'TaskEdit',
    component: TaskEdit,
    props: true,
    meta: { requiresAuth: true },
  },
  {
    path: '/login',
    name: 'UserLogin',
    component: UserLogin,
  },
  {
    path: '/register',
    name: 'RegisterUser',
    component: RegisterUser,
  },
  {
    path: '/logout',
    name: 'UserLogout',
    component: UserLogout,
    meta: { requiresAuth: true },
  },
  {
    path: '/:pathMatch(.*)*',
    name: 'NotFound',
    component: NotFound,
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

// Vue Router Guard untuk pengecekan otentikasi
router.beforeEach((to, from, next) => {
  const isAuthenticated = !!localStorage.getItem('authToken');

  if (to.matched.some(record => record.meta.requiresAuth)) {
    // Rute memerlukan otentikasi, periksa status otentikasi pengguna
    if (!isAuthenticated) {
      // Jika pengguna belum login, arahkan ke halaman login atau respons lain
      next('/login'); // Ganti '/login' dengan rute login Anda
    } else {
      // Jika pengguna sudah login, lanjutkan navigasi
      next();
    }
  } else {
    // Rute tidak memerlukan otentikasi, lanjutkan navigasi
    next();
  }
});

export default router
